﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        protected Product SelectedProduct;

        protected List<Product> Products = new List<Product>();

        protected List<Product> ProductsInOrder = new List<Product>();

        public Form1()
        {
            InitializeComponent();

            this.Text = "Оформление заказа";

            this.LoadProductItems();
        }

        protected void LoadProductItems() 
        {
            if (this.openFileProducts.ShowDialog() != DialogResult.OK) 
            {
                MessageBox.Show(
                    "Не выбран файл с товарами.",
                    "Ошибка!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );

                Environment.Exit(-1);
            }

            byte[] array = null;

            try
            {
                StreamReader fileProducts = new StreamReader(this.openFileProducts.FileName, Encoding.UTF8);


                while (!fileProducts.EndOfStream)
                {
                    string productLine = fileProducts.ReadLine();

                    string[] tempAr = productLine.Split('|');

                    Product product = new Product(
                        tempAr[0],
                        Int32.Parse(tempAr[1]),
                        tempAr[2],
                        tempAr[3]
                    );

                    this.productSelect.Items.Add(product.Name);
                    this.Products.Add(product);
                }

                fileProducts.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    ex.Message,
                    "Ошибка!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );

                Environment.Exit(-1);
            }

        }

        private void AddToOrderButtonClick(object sender, EventArgs e)
        {
            if (this.SelectedProduct == null) 
            {
                return;
            }
            this.ProductsInOrder.Add(this.SelectedProduct);
            this.ReloadOrderView();
            this.ReloadTotalPriceView();
        }

        private void ProductSelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;

            int SelectedProductId = comboBox.SelectedIndex;

            this.SelectedProduct = this.Products[SelectedProductId];

            this.ReloadSelectedProductView();
        }

        private void ReloadSelectedProductView() 
        {
            this.Productdesc.Text = SelectedProduct.Description;
            this.ProductPrice.Text = SelectedProduct.Price.ToString();
            this.ProductManufacturer.Text = SelectedProduct.Manufacturer;
        }

        private void ReloadOrderView() 
        {
            string orderText = "";

            foreach (Product item in this.ProductsInOrder)
            {
                orderText += $"{item.Name}\n{item.Price} р.\n\n";
            }

            orderTextBox.Text = orderText;
        }
        
        private void ReloadTotalPriceView()
        {
            int totalPrice = 0;

            foreach (Product item in this.ProductsInOrder)
            {
                totalPrice += item.Price;
            }

            if (totalPrice > 0) 
            {
                this.totalPriceTextBox.Text = totalPrice.ToString() + " руб.";
            }
            else
            {
                this.totalPriceTextBox.Text = "";
            }
        }
        
        private void NewOrderButtonClick(object sender, EventArgs e)
        {
            this.ProductsInOrder = new List<Product>();
            this.ReloadOrderView();
            this.ReloadTotalPriceView();
        }

        private void SaveOrderButtonClick(object sender, EventArgs e)
        {
            if (ProductsInOrder.Count <= 0)
            {
                MessageBox.Show(
                    "Ни один товар не выбран.",
                    "Ошибка!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );
                return;
            }

            if (this.saveFileOrder.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            try
            {
                FileStream fileOrder = File.OpenWrite(this.saveFileOrder.FileName);

                string result = "";

                foreach (Product item in this.ProductsInOrder)
                {
                    result += $"{item.Name} - {item.Price} руб.\n\n";
                }

                byte[] array = Encoding.Default.GetBytes(result);

                fileOrder.Write(array, 0, array.Length);
                fileOrder.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    ex.Message,
                    "Ошибка!",
                    MessageBoxButtons.OK
                );
                return;
            }

            MessageBox.Show("Файл с заказом сохранён.",
                "Успешно!",
                MessageBoxButtons.OK
            );
        }
    }

    public class Product
    {
        public string Name { get; }

        public int Price { get; }

        public string Description { get; }

        public string Manufacturer { get; }

        public Product(string name, int price, string description, string manufacturer)
        {
            this.Name = name;
            this.Price = price;
            this.Description = description;
            this.Manufacturer = manufacturer;
        }


    }
}
