﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.addToOrderButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ProductManufacturer = new System.Windows.Forms.Label();
            this.ProductPrice = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.descriptionLable = new System.Windows.Forms.Label();
            this.Productdesc = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.productSelect = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.orderTextBox = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.totalPriceTextBox = new System.Windows.Forms.RichTextBox();
            this.newOrderButton = new System.Windows.Forms.Button();
            this.saveOrderButton = new System.Windows.Forms.Button();
            this.openFileProducts = new System.Windows.Forms.OpenFileDialog();
            this.saveFileOrder = new System.Windows.Forms.SaveFileDialog();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // addToOrderButton
            // 
            this.addToOrderButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.addToOrderButton.Location = new System.Drawing.Point(50, 298);
            this.addToOrderButton.Name = "addToOrderButton";
            this.addToOrderButton.Size = new System.Drawing.Size(235, 33);
            this.addToOrderButton.TabIndex = 0;
            this.addToOrderButton.Text = "Добавить товар в заказ";
            this.addToOrderButton.UseVisualStyleBackColor = true;
            this.addToOrderButton.Click += new System.EventHandler(this.AddToOrderButtonClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ProductManufacturer);
            this.groupBox1.Controls.Add(this.ProductPrice);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.descriptionLable);
            this.groupBox1.Controls.Add(this.Productdesc);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.productSelect);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(325, 270);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Характеристики";
            // 
            // ProductManufacturer
            // 
            this.ProductManufacturer.AutoSize = true;
            this.ProductManufacturer.Location = new System.Drawing.Point(111, 109);
            this.ProductManufacturer.Name = "ProductManufacturer";
            this.ProductManufacturer.Size = new System.Drawing.Size(0, 13);
            this.ProductManufacturer.TabIndex = 7;
            // 
            // ProductPrice
            // 
            this.ProductPrice.AutoSize = true;
            this.ProductPrice.Location = new System.Drawing.Point(95, 76);
            this.ProductPrice.Name = "ProductPrice";
            this.ProductPrice.Size = new System.Drawing.Size(0, 13);
            this.ProductPrice.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Изготовитель:";
            // 
            // descriptionLable
            // 
            this.descriptionLable.AutoSize = true;
            this.descriptionLable.Location = new System.Drawing.Point(24, 141);
            this.descriptionLable.Name = "descriptionLable";
            this.descriptionLable.Size = new System.Drawing.Size(57, 13);
            this.descriptionLable.TabIndex = 4;
            this.descriptionLable.Text = "Описание";
            // 
            // Productdesc
            // 
            this.Productdesc.Location = new System.Drawing.Point(27, 157);
            this.Productdesc.Name = "Productdesc";
            this.Productdesc.ReadOnly = true;
            this.Productdesc.Size = new System.Drawing.Size(257, 102);
            this.Productdesc.TabIndex = 3;
            this.Productdesc.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Стоимость:";
            // 
            // productSelect
            // 
            this.productSelect.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.productSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.productSelect.FormattingEnabled = true;
            this.productSelect.Location = new System.Drawing.Point(71, 32);
            this.productSelect.Name = "productSelect";
            this.productSelect.Size = new System.Drawing.Size(213, 21);
            this.productSelect.TabIndex = 1;
            this.productSelect.SelectedIndexChanged += new System.EventHandler(this.ProductSelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Товар:";
            // 
            // orderTextBox
            // 
            this.orderTextBox.Location = new System.Drawing.Point(357, 41);
            this.orderTextBox.Name = "orderTextBox";
            this.orderTextBox.ReadOnly = true;
            this.orderTextBox.Size = new System.Drawing.Size(205, 158);
            this.orderTextBox.TabIndex = 4;
            this.orderTextBox.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(365, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Заказ:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(359, 213);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "ИТОГО:";
            // 
            // totalPriceTextBox
            // 
            this.totalPriceTextBox.Location = new System.Drawing.Point(357, 229);
            this.totalPriceTextBox.Name = "totalPriceTextBox";
            this.totalPriceTextBox.ReadOnly = true;
            this.totalPriceTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Horizontal;
            this.totalPriceTextBox.Size = new System.Drawing.Size(205, 33);
            this.totalPriceTextBox.TabIndex = 7;
            this.totalPriceTextBox.Text = "";
            // 
            // newOrderButton
            // 
            this.newOrderButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.newOrderButton.Location = new System.Drawing.Point(357, 259);
            this.newOrderButton.Name = "newOrderButton";
            this.newOrderButton.Size = new System.Drawing.Size(205, 33);
            this.newOrderButton.TabIndex = 8;
            this.newOrderButton.Text = "Новый заказ";
            this.newOrderButton.UseVisualStyleBackColor = true;
            this.newOrderButton.Click += new System.EventHandler(this.NewOrderButtonClick);
            // 
            // saveOrderButton
            // 
            this.saveOrderButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.saveOrderButton.Location = new System.Drawing.Point(357, 298);
            this.saveOrderButton.Name = "saveOrderButton";
            this.saveOrderButton.Size = new System.Drawing.Size(205, 33);
            this.saveOrderButton.TabIndex = 9;
            this.saveOrderButton.Text = "Сохранить заказ";
            this.saveOrderButton.UseVisualStyleBackColor = true;
            this.saveOrderButton.Click += new System.EventHandler(this.SaveOrderButtonClick);
            // 
            // openFileProducts
            // 
            this.openFileProducts.FileName = "openFileProducts";
            this.openFileProducts.Filter = "\"Dat файлы (*.dat)|*.dat";
            // 
            // saveFileOrder
            // 
            this.saveFileOrder.Filter = "\"Текстовые файлы (*.txt)|*.txt";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(575, 340);
            this.Controls.Add(this.saveOrderButton);
            this.Controls.Add(this.newOrderButton);
            this.Controls.Add(this.totalPriceTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.orderTextBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.addToOrderButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addToOrderButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label descriptionLable;
        private System.Windows.Forms.RichTextBox Productdesc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox productSelect;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox orderTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox totalPriceTextBox;
        private System.Windows.Forms.Button newOrderButton;
        private System.Windows.Forms.Button saveOrderButton;
        private System.Windows.Forms.Label ProductManufacturer;
        private System.Windows.Forms.Label ProductPrice;
        private System.Windows.Forms.OpenFileDialog openFileProducts;
        private System.Windows.Forms.SaveFileDialog saveFileOrder;
    }
}

